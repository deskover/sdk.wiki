## What is a Selector?
A selector is basically a plain text query that is used to find a particular GUI element among the running applications. A selector is actually an XML fragment specifying a few attributes of the GUI element you are looking for and of some of its parents. It is, if you will, very similar to the notion of locating an element into an XML document using an xpath query. We have chosen to have our own simplified syntax instead of the very complex jquery that can sometimes be hard to read.

Selectors can be generated automatically by the _Selection_ tool in **UiExplorer**, but you can also write them manually. If you want to create a customized selector, we recommend starting from an automatically generated selector and then change a few of the attributes.

## Why Selectors? 
Screen scraping and GUI automation works of course with graphical user interface elements. These elements usually change position between different runs of their application because of many factors that are out of your control: Windows being resized and moved, OS version, Screen color and resolution, user preferences, framework versions etc…

A selector is a mean to find the target element regardless of its position on the screen. The selector uses only attributes that are independent of the GUI element's position on the screen.

Also a selector is a useful mean to separate the code that deals with GUI elements by their representation. Using most libraries one would write this code to type some text in notepad:
```cpp
GUIElement gui = new GUIElement();
Gui.FindWindow(‘title = “Notepad”’).Edit.Type(“mytext”);
```
Nothing wrong with that, but if the title changes from Notepad to MyNotepad you need to come back to your code, change it and recompile it. Just imagine having 100s scripts that use an outdated title.

Using selectors you can write:
```javascript
String selector = “<wnd app='notepad.exe' cls='Notepad' title='Untitled - Notepad'/>
<wnd cls='Edit'/>
<ctrl role='editable text'/>”

UINode edit = UiFactory.NewUINode();
edit.FromSelector(selector);

edit.WriteText(1, “mytext”);
```

Now imagine that instead of setting the selector in the source code you load it from a database:
```javascript
String selector = LoadNotepadSelectorFromDatabase();
```
When the app changes you only need to change the selector in the database.


## Syntax

A selector has the following format:
```xml
<node_1><node_2>...<node_N>
```
Each node represents a parent of the desired GUI element, last one representing the element itself.

```<node_1>``` is the root of all the nodes. ```<node_K>``` is an ancestor of ```<node_(K+1)>```, but not necessarily a direct parent. ```<node_(K+1)>``` uniquely describes the child node in the context of the entire sub-tree of ```<node_K>```.

Each node has the following format:
```<ui_system attr_name_1='attr_value_1' ... attr_name_N='attr_value_N'/>```

The starting "<" and ending "/>" sequences are part of the syntax. They delimit the node identifiers.

### UI systems and attributes
ui_system represents the user interface technology that is used to create the specific node. Better to understand with an example: Imagine a java application. Every java application has a toplevel window. The selector starts naturally from this top level window that is created using Windows API thus the first node is something like this: <wnd app="javaw.exe" cls="SunAwtFrame" title="Java Control Panel"/>. Now inside this window we have a tab control with few buttons. The tab and the buttons are created using java Swing framework. Their node in selector is <java name="General" role="page tab list"/>

Here is a complete list of all the supported UI systems and their specific attributes:

#### WND - Win32 window object

The parent nodes of these nodes can only be **wnd** nodes.

They can be root nodes.

The specific attributes for a Win32 window are:

 **app**    - the name of the application which owns the window. It is the lowercase name of the EXE.
        - only the root node (see _<node_1>_ above) can have this attribute. It is ignored in child nodes.
         _Example:_ app="notepad.exe".
 
 **cls**    - the class name of the window. 
         _Example:_ cls='Edit'.
 
 **title**  - the title or text content of the window.
         _Example:_ title='Untitled - Notepad'.
 
 **aaname** - a special internal attribute used for better differentiation. The meaning depends on the control implementation.

#### HTML - browser window object 

Nodes which belong to this UI system indicate the Internet Explorer/Firefox/Chrome browser page that normally has the title and url attributes. They are usually the root node for **webctrl** nodes, which are described below.

The parent nodes for this type of node can only be **wnd**. 

They can be root nodes.

The valid attributes for the **html** nodes are:

 **app**   - the name of the process which owns the window. 
       - if this attribute is missing, the **default value of "iexplore.exe"** is taken.
 
 **title** - the page title as it appears in the browser caption area.
        _Example:_ title='Yahoo!'.
 
 **url**   - the URL pf the page as it appears in the address bar (if any). 
        _Example:_ url='http://www.yahoo.com/'.

#### WEBCTRL - Web control 

These nodes describe HTML elements inside an Internet Explorer browser window.

The parent nodes can be **webctrl** or **html**, but there has be exactly one **html** parent.

As a result, they cannot be root nodes.

The attributes for a Web control are the following:

 **tag**  - the tag name of the HTML element.
       _Example:_ tag='DIV'.
 
 **type** - the type of the HTML element.
       _Example:_ type='BUTTON'.

#### CTRL - generic control 

This is an internal interpretation of controls and it depends on the control implementation.

The parent nodes can be **ctrl** or **wnd**, but there has to be at least one **wnd** parent.

In consequence, they cannot be root nodes.

The valid attributes are:

 **name** - the name of the control.
       _Example:_ name='Manufacturer'.
 
 **role** - the role of the control.
       _Example:_ role='combobox'.

#### JAVA - Java specific control 

This indicates a UI element which is part of a Java window.

The parent nodes can be **java** or **wnd**, but there has to be at least one **wnd** parent.

In consequence, they cannot be root nodes.

The valid attributes are:
 
 **name** - the name of the control.
 
 **role** - the role of the control.

#### SAP- SAP specific control 

This indicates a UI element which is part of a SAP application.

It always has a **wnd** top parent so, in consequence, they cannot be root nodes.

The valid attributes are:

 **id** - the id of the control provided by SAP GUI scripting.

 **tableCol** - the column index when the object is a cell inside a table/grid.

 **tableRow** - the row index when the object is a cell inside a table/grid.

 **colName** - the column name when the object is a cell inside a table/grid.

#### Silverlight - Silverlight specific control 

This indicates a UI element which is part of a Silverlight application.

It has a **webctrl** parent (with tag='object') so, in consequence, they cannot be root nodes.

The valid attributes are:

 **role** - the role of the object.

 **name** - the name of the object.

 **text** - the text of the object.

### NAV - navigation pseudo-control

This is not a real control tag. It specifies navigation from the current node (for now only **up** navigation is supported). It cannot appear at the beginning of a selector because there is no current node yet. The parent nodes can be anything. It is supported in [[FromSelector|UiNode#wiki-FromSelector]] and [[FindFirst|UiNode#wiki-FindFirst]] methods.

The valid attributes are:
 
 **up** - a number that specifies how many parents to go up in the UI hierarchy.

 **next** - a number that specifies how many siblings to go forward in the UI hierarchy.

 **prev** - a number that specifies how many siblings to go backward in the UI hierarchy.

### The INDEX attribute 

Sometimes, the desired UI element does not provide enough information to make it unique in the context of its node hierarchy. In this case, a special attribute named **index** is used to reach the node, and in consequence the UI element.

The syntax is:

 **idx**="numeric_value"

_Example:_ idx='3'.

If the numeric value is not specified, the default value is '1'.

In the sequence **"<wnd app='myapp.exe'/><wnd cls='Edit' idx='5'/>"**, we will look for the 5-th node with the **cls** attribute set to 'Edit' in the whole sub-tree of the parent node **"<wnd app='myapp.exe'/>"**.

If the **idx** attribute is specified for the root node as in **"<wnd cls='Explorer' idx='5'/>"**, it will indicate the 5-th top-level window in the internal enumeration which has the **cls** attribute set to "Explorer".

This attribute applies to all UI systems.

### Wildcards: * and ? 

Sometimes, attributes change their values from time to time due to the fact that the application's UI changes. When this happens, part of the dynamic attributes often remain unchanged and we have to specify the variable parts somehow. 

This can be done using _wildcards_:

* star means zero or more characters can be there. _Example:_ title='* - Notepad'.
 
**?** question mark means exactly one character. _Example:_ title='Expl?rer'.

A direct effect of using wildcards is the increasing probability of there being more and more nodes matching the Selector. That's why, in order to uniquely identify a node, we might have to add an **idx** attribute.

_Example 1:_ we have two windows with the following titles: **Report <current_time>** and **Status <current_date>**. The variable parts can be easily noticed. We want to choose the second value, and somehow our original identifier ```<wnd cls='AfxWnd' title='Status 7/10/2010'/>``` doesn't work anymore. We can reach it using wildcards like so: ```<wnd cls='AfxWnd' title='Status *'/>```.

_Example 2:_ we have three windows with the following titles: **Report <current_time>**, **Status <current_date>** and **Status <tomorrow_date>**. 
We can reach the third window with: ```"<wnd cls='AfxWnd' title='Status *' idx='2'/>"```.

Most attributes support wildcards.