__TOC__

===Find activities===
In order to find a particular activity in Activities' toolbox you have to:
 Type the name of the activity in the search field on top of activities' toolbox. 
 Search results are shown as you type.

'''''Known issue:''''' If you have manually collapsed a category then its activities are not shown in search. Expand it to workaround this issue.
[[images/Search.png]]

===Breadcrumbs navigation===

By default workflow show the entire activities hierarchy but you can drill down to a specific sequence of activities or even a single activity.

 Double-click an activity to change the root activity to this one. 

The clicked activity is then fully-expanded at the root and its ancestors are shown in the breadcrumb bar. This is sometimes called drilling in or out of an activity.

'''''TIP:''''' To navigate to an ancestor of the current root activity, click the activity in the breadcrumb bar.

Learn more about using breadcrumbs navigation [http://msdn.microsoft.com/en-us/library/ee829531.aspx here]

[[images/Breadcrumbs.png]]

===Delete an activity===
In order to delete an activity you have two options:
#just click activity's name bar and then press ''Del'' key 
#right click on activity's screenshot and choose ''Delete''(see picture below). ''Note: By clicking directly on activity's name you will enter edit mode and you can change the activity's name.'' 
[[images/Delete_Undo.png]]

===Undo an action===
Just press ''CTRL+Z''.

===Re-arange activities===
Drag and drop an activity anywhere in the workflow. 
 

