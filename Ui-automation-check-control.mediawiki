[[images/Live.com-CheckButton.png]]

==UI object selector==

A selector is actually an XML fragment specifying few attributes of the GUI element you are looking for and of some of its parents. It is if you want very similar to the notion of locating an element into an XML document using an xpath query. We have chosen to have our own simplified syntax instead of the very complex jquery that sometimes can be hard to read. 

==Edit UI object selector==

By clicking Edit Selector button [[Ui Explorer]] will open.

==Toggle control==

Allows you to check and uncheck the control. If ''toggle'' is not checked, UiPath Studio will check all controls.

With ''toggle'' checked, Studio will uncheck already checked controls or check them if they're not checked.

==Generate source code or JavaScript==

UiPath Studio can generate source code - '''C#, C++, VB.NET, VB6 - or JavaScript''' to automate checking/unchecking controls
