In the Output panel you can see the activity tracing during execution. Besides built-in tracing you can add your own using Write line activity under Debug category. 

In our example below: 
#We clicked '''Debug''',
#Activities are highlighted on designer's surface as they're executed,
#In the output pane you can see details about: 
##the time when the execution of the activity began, 
##name of the activity,
##end of execution.

[[Images/Output.png]]