Steps to follow:

#Open UiPath Studio.
#Open your workflow.
#Click on Deploy tab (see ''Deploy'' picture below).
#Then click on Create app button beneath.
#Check "Include UiPath Runtime" radio button on the new dialog (see ''Create application'' picture below).
#Enter the codes that you received by email at purchase time/trial download to activate the runtime.
#Click Create application button.
#Now copy the created .zip file for your workflow and paste it on a second machine. 
#Double click on it to run your workflow.

''Side note: sometimes it might take a little until it runs. Just wait for a few seconds.''

'''Deploy'''
[[images/DeployIMG.png]]

'''Create application'''
[[images/createapplication.png]]
