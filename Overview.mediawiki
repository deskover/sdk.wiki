===Introduction===
UiPath is a complete solution for application integration, automating third party software, automating administrative IT tasks and for automating business IT processes. Intended audience are IT professionals helping their organisation eliminate repetitive, manual processes and developers looking to use this technology from their programs.

UiPath comes with [[UiPath Studio]], an integrated development environment and a software [[UiPath framework|framework]] for creating automation applications.

Central to UiPath is the notion of [[what is workflow|workflow]]. A workflow models an automation process as a set of [[workflow activities|activities]]. Each activity encapsulates an atomic action like clicking a button, reading a file or an API call. As such, a workflow describes the order of execution and dependency relationships between its activities.

You don't have to be a programmer to create an automation app. UiPath Studio provides a state of the art workflow designer where you drag and drop activities to compose a workflow. Workflows can be packed with UiPath [[runtime]] into a single executable for easy [[deployment]] on other computers.

UiPath comes bundled with a large collection of built-in activities covering Application Integration, GUI Automation, Screen Scraping, Data Entry, Databases, Excel, Web Scraping, Web Automation, SOAP/REST API, PowerShell and many others and you can download others from our online [[gallery]].

Here is a diagram that depicts UiPath high-level [[architecture]].

===Quick start with UiPath Studio===
We recommend to watch the 5 minutes online video [[Tutorial|tutorial]] to get started using [[UiPath Studio]]. 

===Developing with UiPath Framework===
UiPath has been designed from ground up to be a software [[UiPath framework|framework]] for developing automation software  applications. It is developed on the top of .Net framework and Windows workflow foundation thus .Net developer doesn't have to leave her couch to [[start developing]] but the outcast is not gloom either for other languages and environments. 

GUI Automation and screen scraping play an important role in automation software. UiPath provides a dedicated [[GUI Automation API]] to get full control of third-party apps GUI.

You can find [[here]] the starting development guide.

===Professional Services===
If you are unfamiliar with application integration, automation software, screen scraping, UI automation and how to use UiPath development tools, you can contact sales@deskover.com and we do have professional services offerings available where we can solve your requirements.