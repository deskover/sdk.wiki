===Overview===
This tool produces training files for tesseract engine that are optimized for specified fonts and character set.

===Save configuration file===
Save current configuration to be used at a later time.

===Open configuration file===
Load a previously saved configuration file.

===Language===
This will actually be the name of the resulting training file.

===Training text===
Best practices recommends to combine plain words with symbols. 

===Fonts===
Add/edit the fonts subject to training.
*Font name specifies family font name.
*Font style specifies style (Ex: regular, bold, italic)

===Start training===
Starts training process. It might take a couple of minutes.
