__TOC__
This section lists in alphabetical order the classes and constants that make up the UiPath GUI Automation API.

==Library==
'''UiPath.dll''' is the internal name of the API and the name of the file that contains definitions of classes that make up the API. 
You need to add a reference to it and specify the namespace to get the definitions.

*;C#
Add a reference to '''UiPath.Interop.dll''' prior to ''using'' it.
<source lang="csharp">
using UiPath;
</source>

*;VB.Net
Add a reference to '''UiPath.Interop.dll''' then ''import'' definitions.
<source lang="vbnet">
Imports UiPath
</source>

*;VB6
Just add a reference to '''UiPathLib'''.

*;C++
```cpp
#import "UiPath.dll" named_guids
using namespace UiPathLib;
```

==Classes==
;*[[UiBrowser]]
;*[[UiEventInfo]]
;*[[UiFactory]]
;*[[UiImage]]
;*[[UiNode]]
;*[[UiNodeMonitor]]
;*[[UiRegion]]
;*[[UiScrapeOptions]]
;*[[UiScrapeResult]]
;*[[UiSystem]]
;*[[UiTextInfo]]
;*[[UiWindow]]

==Constants==
;*[[UiBrowser#wiki-UiBrowserType__DEPRECATED|UiBrowserType]]
;*[[UiNode#wiki-UiClickType|UiClickType]]
;*[[UiRegion#wiki-UiDirection|UiDirection]]
;*[[UiNodeMonitor#wiki-UiEventMode|UiEventMode]]
;*[[UiNodeMonitor#wiki-UiEventType|UiEventType]]
;*[[UiNode#wiki-UiFindScope|UiFindScope]]
;*[[Uinode#wiki-UiInputMethod|UiInputMethod]]
;*[[UiNodeMonitor#wiki-UiKeyModifier|UiKeyModifier]]
;*[[UiNode#wiki-UiMouseButton|UiMouseButton]]
;*[[UiScrapeOptions#wiki-UiOCREngine|UiOCREngine]]
;*[[UiScrapeOptions#wiki-UiScrapingMethod|UiScrapingMethod]]
;*[[UiNode#wiki-UiSelectionType|UiSelectionType]]
;*[[UiNode#wiki-UiWaitForReadyLevels|UiWaitForReadyLevels]]

==[[Error-Codes|Error Codes]]==