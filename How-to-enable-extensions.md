###Chrome

**Option I - enabling UiPath extension from UiPath Studio**
* Open UiPath Studio and click Setup button on command bar (see SetupExtensions picture below).
* Click Setup Extensions button and choose Chrome.
* Click +Free green button in the Chrome Web Store window for UiPath - upper right side of the window (see AddUiPathExtension picture below).
* Confirm addition by clicking Add button on the next confirmation dialog (see Confirm image below). A new window opens confirming succssesful (restarting Chrome is required to complete installation.)


Setup Extensions [[images/SetupExtensions.png]]

Adding UiPath extension...[[images/AddUiPathExtension.png]]

Confirm

[[images/Confirm.png]]


UiPath Extension Added[[images/added_to_chrome.png]]


**Option II - enable UiPath extension from cmd**
* Click Windows Start button and type cmd in the search field.
* Right click on cmd.exe and run it as administrator.
* Enter the path to SetupExtensions.exe /chrome. SetupExtensions.exe file is located in UiPath subfolder in UiPath installation directory (C:\Program Files (x86)\UiPath Studio\v7\UiPath). 

###Firefox

**Option I**

Firefox asks you to enable UiPath plugin. Once you click the enable button you will have to restart Firefox to complete the process. You can also enable UiPath plugin any time later by following these steps:
* Open Firefox
* Open Extensions page by pressing at once **CTRL+SHIFT+A** keys
* Click Enable button on the right next to UiPath plugin (see the picture below)

[[images/EnablePluginFireFox.png]]

OR

**Option II**

As above, just replace Chrome with Firefox - SetupExtensions.exe /Firefox